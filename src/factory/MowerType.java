package factory;

/**
 *
 * @author Nicola
 *
 * This is an enumeration fo all the types of Entities
 *
 */
public enum MowerType {

	PLAYER, WEED, FUEL, ROCK, FLOWER, BLOCK, GRAVEL, SGRAVEL, GRASS, WATER

}
